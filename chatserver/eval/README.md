# Evaluations

This directory contains evaluation sets (or instructions on how to use them) for AI assistants.

## Mini Evals

A curated selection of evaluation sets. These sets are compact yet representative, enabling humans to review all answers manually with ease.

# Auto Evaluation

## Evaluate QA with ChatGPT

See `eval_qa_chatgpt.py`

# Generate baseline QA with ChatGPT

See `qa_baseline_gpt-3.5-turbo.py`
